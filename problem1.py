checkUpTo = 1000;

divisors = [3, 5]

sum = 0

for i in range(checkUpTo):
    for j in range(len(divisors)):
        if i % divisors[j] == 0: sum += i
print(sum)
