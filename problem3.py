#check if a number is prime or not:
def isPrime(n):
    if n == 1: return False
    if n == 2: return True
    if n % 2 == 0: return False
    for i in range(2, n):
        if n % i == 0: return False
    return True

#calculate the product of an array:
def productArray(n):
    product = 1
    for i in range(len(n)):
        product *= n[i]
    return product

#the number we want the largest prime factor from:
target = 600851475143
#array to store all prime factors:
primeFactors = []

for i in range(1, target):
    if isPrime(i):#if we found a prime number
        if target % i == 0:#check if it is a factor of our target
            primeFactors.append(i)#if so, add it to the array
            if(productArray(primeFactors) == target):#if we found all prime factors
                print(i)#print the largest
                break
            
